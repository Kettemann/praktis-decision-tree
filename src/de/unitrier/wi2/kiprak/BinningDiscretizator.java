package de.unitrier.wi2.kiprak;

import java.util.*;

/**
 * Implements discretization for non-numeric values.
 */
public class BinningDiscretizator implements IDiscretizatior {

    /**
     * Uses equal width binning to discretize a specified non-numeric attribute.
     * This means that the value spectrum is divided into bins with an equal range of values.
     * The range of a bin is defined by: (max - min) / (number of bins).
     *
     * @param numberOfBins defines how many bins should be used for discretization.
     * @param examples     the collection of CSV rows that contains non-numeric values.
     * @param attributeId  the index of the attribute that should be discretized.
     * @param <T>          the generic type parameter of the data.
     * @return the examples list with the specified attribute being discretized.
     */
    @Override
    public <T extends Comparable<T>> Collection<T[]> discretize(int numberOfBins, Collection<T[]> examples, int attributeId) {
        Collection<T[]> linkedList = new LinkedList<>();
        ArrayList<Double> doubles = tArrayToDoubles(examples, attributeId);
        int[] indices = new int[numberOfBins + 1];
        for (int i = 0; i <= numberOfBins; i++) {
            indices[i] = 0;
        }

        double min = Collections.min(doubles);
        double max = Collections.max(doubles);

        double d;
        for (T[] arr : examples) {
            try {
                d = Double.parseDouble(arr[attributeId].toString());
            } catch (Exception e) {
                // invalid values will be skipped
                continue;
            }
            for (int j = 1; j < numberOfBins + 1; j++) {
                double iterationMaxValue = min + j * ((max - min) / numberOfBins);
                if (iterationMaxValue >= d) {
                    String attributeValue = "I" + j + ": " + iterationMaxValue;
                    arr[attributeId] = (T) attributeValue;
                    linkedList.add(arr);
                    indices[j]++;
                    break;
                }
            }
        }

        return linkedList;
    }

    /**
     * Transform a generic T array into an array of doubles.
     * Implemented by parsing a double value from the T values toString() method.
     *
     * @param examples    the collection of CSV rows that contains non-numeric values.
     * @param attributeId the index of the attribute that should be discretized.
     * @param <T>         the generic type parameter of the data.
     * @return an ArrayList of doubles.
     */
    public static <T extends Comparable<T>> ArrayList<Double> tArrayToDoubles(Collection<T[]> examples, int attributeId) {
        ArrayList<Double> doubles = new ArrayList<>();
        for (T[] arr : examples) {
            try {
                double d = Double.parseDouble(arr[attributeId].toString());
                doubles.add(d);
            } catch (Exception e) {
            } // skip invalid attributes
        }
        return doubles;
    }


}