package de.unitrier.wi2.kiprak;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Contains fileIO for CSV files.
 */
public class CSVReader {

    /**
     * Reads a CSV file.
     *
     * @param path         the path to the CSV file.
     * @param delimiter    the delimiter which separates values in a CSV row.
     * @param ignoreHeader defines whether to include the CSV header row or not.
     * @return a list of CSV rows.
     * @throws IOException
     */
    public static List<String[]> readCsvToArray(String path, String delimiter, boolean ignoreHeader) throws IOException {

        List<String[]> records = new LinkedList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            if(ignoreHeader) br.readLine(); // ignore the first line
            String line;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(delimiter);
                records.add(values);
            }
        }

        return records;
    }

    /**
     * Reads the CSV header row.
     *
     * @param path      the path to the CSV file.
     * @param delimiter the delimiter which separates values in a CSV row.
     * @return a list of the CSV column names.
     */
    public static LinkedList<String> readCsvHeadersToArray(String path, String delimiter) {

        LinkedList<String> records = new LinkedList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line= br.readLine();

            if (line != null) {
                String[] values = line.split(delimiter);
                records = new LinkedList<>(Arrays.asList(values));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return records;
    }
}
