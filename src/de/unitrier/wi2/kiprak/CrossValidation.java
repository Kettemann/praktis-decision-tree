package de.unitrier.wi2.kiprak;

import java.util.*;

import static de.unitrier.wi2.kiprak.State.*;

/**
 * Implements n-fold cross validation.
 */
public class CrossValidation {

    /**
     * Validate the results of the ID3 algorithm implemented in the ID3 class.
     *
     * @param n      number of training and test cycles used for validation.
     * @param matrix matrix of the training data (example data), e.g. ArrayList<String[]>.
     * @param <T>    the generic type parameter of the data.
     * @return the validation results.
     */
    public static <T extends Comparable<T>> double nFoldValidation(int n, Collection<T[]> matrix, boolean usePruning) {
        LinkedList<T[]> data = new LinkedList<>(matrix);
        if (CROSS_VALIDATION_SHUFFLE_TESTDATA) {
            Collections.shuffle(data);
        }
        LinkedList<LinkedList<T[]>> testData = getTestData(n, data);
        LinkedList<LinkedList<T[]>> trainingData = getTrainingData(n, testData);
        HashMap<Integer, Integer[]> resultMap = new HashMap<>();
        double overallResult = 0.0;

        // initialize result HashMap
        for (int i = 0; i < n; i++) {
            resultMap.put(i, new Integer[]{0, 0});
        }

        for (int i = 0; i < n; i++) {
            DecisionTreeNode rootNode = null;
            if (usePruning) {
                rootNode = new ReducedErrorPruner().prune(trainingData.get(i));
            } else {
                rootNode = ID3.createTree(trainingData.get(i), LABEL_INDEX);
            }
            double result = testResults(testData.get(i), rootNode);
            overallResult += (1.0 / n) * result;
        }

        return overallResult;
    }

    /**
     * Filter every nth value from the matrix collection as testData.
     *
     * @param n      number of training and test cycles used for validation.
     * @param matrix matrix of the training data (example data), e.g. ArrayList<String[]>.
     * @param <T>    the generic type parameter of the data.
     * @return the testData.
     */
    public static <T extends Comparable<T>> LinkedList<LinkedList<T[]>> getTestData(int n, Collection<T[]> matrix) {
        LinkedList<LinkedList<T[]>> testData = new LinkedList<>();

        // initialize linkedLists for splitting
        for (int i = 0; i < n; i++) {
            testData.add(new LinkedList<>());
        }

        // fill the linkedList with values
        Iterator<T[]> iterator = matrix.iterator();
        for (int i = 0; i < matrix.size(); i++) {
            T[] item = iterator.next();
            testData.get(i % n).add(item);
        }
        return testData;
    }

    /**
     * Remove every nth value from the matrix collection as trainingData.
     *
     * @param n        number of training and test cycles used for validation.
     * @param testData the data to exclude from the data set.
     * @param <T>      the generic type parameter of the data.
     * @return the trainingData.
     */
    public static <T extends Comparable<T>> LinkedList<LinkedList<T[]>> getTrainingData(int n, LinkedList<LinkedList<T[]>> testData) {
        LinkedList<LinkedList<T[]>> trainingData = new LinkedList<>();

        // initialize linkedLists for splitting
        // initialize result HashMap
        for (int i = 0; i < n; i++) {
            trainingData.add(new LinkedList<>());
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    trainingData.get(i).addAll(testData.get(j));
                }
            }

        }
        return trainingData;
    }

    /**
     * @param testData the data used for validation.
     * @param rootNode the ID3 decision tree's root node.
     * @param <T>      the generic type parameter of the data.
     * @return the validation results.
     */
    public static <T extends Comparable<T>> double testResults(LinkedList<T[]> testData, DecisionTreeNode rootNode) {
        LinkedList<T[]> resultList = new LinkedList<>();

        int classifiedCorrectly = 0;
        int classifiedWrong = 0;

        for (T[] elem : testData) {
            String result = ID3.ID3GiveMeResults(rootNode, elem);
            String label = (String) elem[LABEL_INDEX];
            String[] entries = {result, label};
            resultList.add((T[]) entries);

            if (result.equals(label)) classifiedCorrectly++;
            else classifiedWrong++;
        }

        return ((double) classifiedCorrectly / (double) (classifiedWrong + classifiedCorrectly));
    }
}
