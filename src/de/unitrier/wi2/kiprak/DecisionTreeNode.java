package de.unitrier.wi2.kiprak;

import java.util.HashMap;

/**
 * Represents a node in a decision tree.
 */
public class DecisionTreeNode {
    private int attributeIndex;
    private DecisionTreeNode parent;
    private HashMap<String, DecisionTreeNode> leafs;
    private String leafNodeClass = null;
    private String dominantClass = "";

    /**
     * Creates a DecisionTreeNode for a given attribute.
     * @param attributeIndex the attributeIndex property of a DecisionTreeNode.
     */
    public DecisionTreeNode(int attributeIndex) {
        this.attributeIndex = attributeIndex;
    }

    /**
     * Creates a DecisionTreeNode for a given label.
     * @param leafNodeClass the leafNodeClass property of a DecisionTreeNode.
     */
    public DecisionTreeNode(String leafNodeClass) {
        this.leafNodeClass = leafNodeClass;
        this.dominantClass = leafNodeClass;
    }

    /**
     * Creates a DecisionTreeNode for a given attribute and label.
     * @param attributeIndex the attributeIndex property of a DecisionTreeNode.
     * @param leafNodeClass the leafNodeClass property of a DecisionTreeNode.
     */
    public DecisionTreeNode(int attributeIndex, String leafNodeClass) {
        this.attributeIndex = attributeIndex;
        this.leafNodeClass = leafNodeClass;
    }

    public void setParent(DecisionTreeNode parent) {
        this.parent = parent;
    }

    public int getAttributeIndex() {
        return attributeIndex;
    }

    public DecisionTreeNode getParent() {
        return parent;
    }

    public String getLeafNodeClass() {
        return leafNodeClass;
    }

    public HashMap<String, DecisionTreeNode> getLeafs() {
        if (leafs == null)
            leafs = new HashMap<String, DecisionTreeNode>();
        return leafs;
    }

    public HashMap<String, DecisionTreeNode> appendLeaf(String s, DecisionTreeNode node) {
        if (leafs == null)
            leafs = new HashMap<String, DecisionTreeNode>();
        leafs.put(s, node);
        return leafs;
    }

    public HashMap<String, DecisionTreeNode> setLeafs(HashMap<String, DecisionTreeNode> leafs) {
        this.leafs = leafs;
        return leafs;
    }

    public String getDominantClass() {
        if (dominantClass == null) return "";
        return dominantClass;
    }

    public void setDominantClass(String dominantClass) {
        this.dominantClass = dominantClass;
    }

    public void setLeafNodeClass(String leafNodeClass) {
        this.leafNodeClass = leafNodeClass;
    }

}
