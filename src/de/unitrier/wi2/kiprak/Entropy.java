package de.unitrier.wi2.kiprak;

import java.util.*;

import static de.unitrier.wi2.kiprak.ID3.getDistinctAttributeValues;

/**
 * Contains entropy calculations necessary for the ID3 implementation.
 */
public class Entropy {

    /**
     * @param matrix         matrix of the training data (example data), e.g. ArrayList<String[]>.
     * @param labelIndex     the index of the attribute that contains the class.
     * @param attributeIndex the index of the attribute for which the information gain is to be calculated.
     * @param <T>            the generic type parameter of the data.
     * @return the attribute's information gain.
     */
    public static <T extends Comparable<T>> double calcInformationGainForAttribute(Collection<T[]> matrix, int labelIndex, int attributeIndex) {
        double entropy = calculateEntropy(countAttributeValueOccurrences(attributeIndex, matrix, null, labelIndex));
        double restEntropyForAttribute = calculateRestEntropyForAttribute(attributeIndex, matrix, labelIndex);

        return entropy - restEntropyForAttribute;
    }

    /**
     * @param n the number to apply the logarithmus dualis to.
     * @return log to basis 2 for n.
     */
    public static double log2(double n) {
        return (Math.log(n) / Math.log(2));
    }

    /**
     * Calculates the entropy for the given parts of the classes. e.g. counts = [3,5] returns entropy 0.954434002924965.
     *
     * @param counts the list containing the sum for each label / value.
     * @return the entropy for the given array.
     */
    public static double calculateEntropy(long[] counts) {
        double positives = 0.0;
        double negatives = 0.0;
        if (counts.length > 0) positives = counts[0];
        if (counts.length > 1) negatives = counts[1];
        if (positives == 0.0 || negatives == 0.0)
            return 0.0;
        double total = positives + negatives;

        double entropy = (-((positives / total) * log2(positives / total))
                - ((negatives / total) * log2(negatives / total))
        );

        return entropy;
    }

    /**
     * @param attributeIndex the index of the attribute for which the entropy is to be calculated.
     * @param matrix         matrix of the training data (example data), e.g. ArrayList<String[]>.
     * @param labelIndex     the index of the attribute that contains the class.
     * @param <T>            the generic type parameter of the data.
     * @return the entropy of an attribute.
     */
    public static <T extends Comparable<T>> double calculateRestEntropyForAttribute(int attributeIndex, Collection<T[]> matrix, int labelIndex) {
        HashMap<T, Integer> attributeValues = getDistinctAttributeValues(matrix, attributeIndex);

        double result = 0.0;
        for (T distinctValue : attributeValues.keySet()) {
            result += ((double) attributeValues.get(distinctValue) / matrix.size()) * calculateEntropyForAttributeValue(attributeIndex, matrix, distinctValue, labelIndex);
        }

        return result;
    }

    /**
     * Beautifully print a Map.
     *
     * @param map the map to be printed to the console.
     */
    public static void printMap(Map map) {
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
    }

    /**
     *
     * @param attributeIndex the index of the attribute for which the entropy is to be calculated.
     * @param matrix         matrix of the training data (example data), e.g. ArrayList<String[]>
     * @param value          attribute value (z.B. 'huge', 'small')
     * @param labelIndex     the index of the attribute that contains the class.
     * @param <T>            the generic type parameter of the data.
     * @return the entropy of an attribute value.
     */
    public static <T extends Comparable<T>> double calculateEntropyForAttributeValue(int attributeIndex, Collection<T[]> matrix, T value, int labelIndex) {
        return calculateEntropy(countAttributeValueOccurrences(attributeIndex, matrix, value, labelIndex));
    }

    /**
     * Counts how often each class appears.
     * This is counted for a specified attribute value
     * or - if this value is null - for all rows in the given matrix.
     *
     * @param attributeIndex the index of the attribute which contains value
     * @param matrix the collection of CSV rows belonging to this part of the tree.
     * @param value the attribute value for which occurrences should be counted. If the value is null, all
     * @param labelIndex     the index of the attribute that contains the class.
     * @param <T>            the generic type parameter of the data.
     * @return an array of how often each attribute value appears in the matrix.
     */
    public static <T extends Comparable<T>> long[] countAttributeValueOccurrences(int attributeIndex, Collection<T[]> matrix, T value, int labelIndex) {

        HashMap<T, Integer> distinctLabels = getDistinctAttributeValues(matrix, labelIndex);
        long[] attributeValueOccurrences = new long[distinctLabels.size()];

        for (T[] t : matrix) {
            if (t[attributeIndex].equals(value) || value == null) {
                int i = 0;
                for (T distinctValue : distinctLabels.keySet()) {
                    if (t[labelIndex].equals(distinctValue)) attributeValueOccurrences[i]++;
                    i++;
                }
            }
        }
        return attributeValueOccurrences;
    }


}