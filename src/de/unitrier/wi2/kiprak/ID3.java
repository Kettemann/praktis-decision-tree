package de.unitrier.wi2.kiprak;

import java.util.*;

import static de.unitrier.wi2.kiprak.Entropy.countAttributeValueOccurrences;

/**
 * ID3 Implementation.
 */
public class ID3 {

    /**
     * Recursively creates the tree.
     *
     * @param examples   the collection of CSV rows belonging to this part of the tree.
     * @param labelIndex the index of the attribute that contains the class.
     * @return the current decision tree node.
     */
    public static <T extends Comparable<T>> DecisionTreeNode createTree(Collection<T[]> examples, int labelIndex) {

        long[] labelOccurrences = countAttributeValueOccurrences(17, examples, null, labelIndex);

        // check if all examples have the same label
        if (labelOccurrences.length == 1) {
            T className = examples.iterator().next()[labelIndex];
            return new DecisionTreeNode(className.toString());
        }

        int efficientAttribute = selectEfficientAttribute(examples, labelIndex);
        DecisionTreeNode thisNode = new DecisionTreeNode(efficientAttribute);
        setNodeMostOften(thisNode, examples, labelIndex);

        for (long labelIncidence :
                labelOccurrences) {
            if (labelIncidence == 0.0)
                return new DecisionTreeNode(efficientAttribute, thisNode.getDominantClass());
        }

        HashMap<T, Integer> attributeValues = getDistinctAttributeValues(examples, efficientAttribute);

        if (attributeValues.size() == 1) {
            // just one distinct attribute value, true partition isn't possible
            return new DecisionTreeNode(-1, thisNode.getDominantClass());
        } else {
            // recursively create subtrees for all occurring attribute values
            for (T attributeValue : attributeValues.keySet()) {
                DecisionTreeNode node = createTree(createPartition(examples, attributeValue, efficientAttribute), labelIndex);
                node.setParent(thisNode);
                thisNode.appendLeaf(attributeValue.toString(), node);
            }
        }
        return thisNode;
    }

    /**
     * Sets the mostOften property of a DecisionTreeNode.
     *
     * @param node      the DecisionTreeNode to which mostOften should be set.
     * @param examples  the collection of CSV rows belonging to this node.
     * @param attribute the attribute this node is belonging to.
     * @param <T>       the generic type parameter of the data.
     */
    public static <T extends Comparable<T>> void setNodeMostOften(DecisionTreeNode node, Collection<T[]> examples, int attribute) {
        HashMap<T, Integer> attributeValues = getDistinctAttributeValues(examples, attribute);

        Integer maximum = Integer.MIN_VALUE;
        T mostOften = null;

        for (T distinctValue : attributeValues.keySet()) {
            if (attributeValues.get(distinctValue) > maximum) {
                maximum = attributeValues.get(distinctValue);
                mostOften = distinctValue;
            }
        }

        node.setDominantClass(mostOften.toString());
    }

    /**
     * Selects the best attribute to continue with the decision tree.
     *
     * @param examples   the collection of CSV rows belonging to this node.
     * @param labelIndex the index of the attribute that contains the class.
     * @param <T>        the generic type parameter of the data.
     * @return the index of the chosen attribute.
     */
    public static <T extends Comparable<T>> int selectEfficientAttribute(Collection<T[]> examples, int labelIndex) {
        int length = examples.iterator().next().length;

        double max = 0;
        int bestIndex = 0;
        for (int i = 0; i < length - 1; i++) {
            double val = Entropy.calcInformationGainForAttribute(examples, labelIndex, i);
            if (val > max) {
                bestIndex = i;
                max = val;
            }
        }
        return bestIndex;
    }

    /**
     * Creates a partition containing only CSV rows with a specific value at a given attributeIndex.
     * This method is called once for each distinct attribute value.
     *
     * @param examples       the collection of CSV rows.
     * @param value          the value for which the partition should be created.
     * @param attributeIndex the index of the csv column specifying the attribute.
     * @param <T>            the generic type parameter of the data.
     * @return the generated partition.
     */
    public static <T extends Comparable<T>> Collection<T[]> createPartition(Collection<T[]> examples, T value, int attributeIndex) {
        List<T[]> list = new LinkedList<>();
        for (T[] t :
                examples) {
            if (t[attributeIndex].equals(value)) {
                list.add(t);
            }
        }

        return list;
    }

    /**
     * Returns all distinct attribute values at a given attributeIndex.
     *
     * @param examples       the collection of CSV rows to be searched.
     * @param attributeIndex the index of the csv column specifying the attribute.
     * @param <T>            the generic type parameter of the data.
     * @return all distinct attribute values.
     */
    public static <T extends Comparable<T>> HashMap<T, Integer> getDistinctAttributeValues(Collection<T[]> examples, int attributeIndex) {
        HashMap<T, Integer> attributeValues = new HashMap<>();
        for (T[] line : examples) {
            int i = 0;
            for (T distinctValue : attributeValues.keySet()) {
                // if the attribute in this line is already added to distinctValues, break the loop
                if (line[attributeIndex].equals(distinctValue)) {
                    attributeValues.put(distinctValue, attributeValues.get(distinctValue) + 1);
                    break;
                }
                i++;
            }
            // if the attribute value went through all iterations, it means that no matching value was in distinctValues -> we can add it to the distinctValues
            if (i == attributeValues.keySet().size()) {
                attributeValues.put(line[attributeIndex], 1);
            }
        }

        return attributeValues;
    }

    /**
     * Counts how often each label appears in the given collection of examples.
     *
     * @param examples   the collection of CSV rows belonging to this part of the tree.
     * @param labelIndex the index of the attribute that contains the class.
     * @param <T>        the generic type parameter of the data.
     * @return the attribute values and the associated number of occurrences.
     */
    public static <T extends Comparable<T>> long[] countOccurrences(Collection<T[]> examples, int labelIndex) {
        HashMap<T, Integer> attributeValues = getDistinctAttributeValues(examples, labelIndex);
        long[] attributeValueOccurrences = new long[attributeValues.size()];

        int i = 0;
        for (T distinctValue : attributeValues.keySet()) {
            attributeValueOccurrences[i] = attributeValues.get(distinctValue);
            i++;
        }

        return attributeValueOccurrences;
    }

    /**
     * Applies the ID3 algorithm to a given example starting from the trees root node.
     *
     * @param rootNode the ID3 tree's root node.
     * @param values   the example where ID3 should be applied to.
     * @param <T>      the generic type parameter of the data.
     * @return the predicted class for the given example.
     */
    public static <T extends Comparable<T>> String ID3GiveMeResults(DecisionTreeNode rootNode, T[] values) {
        while (rootNode.getLeafNodeClass() == null) {
            HashMap<String, DecisionTreeNode> leafs = rootNode.getLeafs();
            boolean hasNodeChanged = false;

            String attributeValue = (String) values[rootNode.getAttributeIndex()];
            int i = 0;
            for (Map.Entry<String, DecisionTreeNode> mapEntry : leafs.entrySet()) {
                i++;
                if (mapEntry.getKey().equals(attributeValue)) {
                    rootNode = mapEntry.getValue();
                    hasNodeChanged = true;
                    break;
                }
            }

            if (i == leafs.size() && !hasNodeChanged) {
                // the attribute was not found
                // -> return the class that is found the most often
                return rootNode.getDominantClass();
            }
        }

        return rootNode.getLeafNodeClass();
    }


    /**
     * Recursively adds all ID3 decision tree nodes to the given linkedList.
     *
     * @param rootNode the ID3 tree's root node.
     * @param nodeList the list to which all nodes should be appended.
     * @param <T>      the generic type parameter of the data.
     * @return the linkedList with appended ID3 decision tree nodes.
     */
    public static <T extends Comparable<T>> LinkedList<DecisionTreeNode> appendAllID3NodesToThisList(DecisionTreeNode rootNode, LinkedList<DecisionTreeNode> nodeList) {
        DecisionTreeNode node = rootNode;
        HashMap<String, DecisionTreeNode> leafs = node.getLeafs();
        for (Map.Entry<String, DecisionTreeNode> mapEntry : leafs.entrySet()) {
            appendAllID3NodesToThisList(mapEntry.getValue(), nodeList);
        }
        nodeList.add(node);
        return nodeList;
    }

}
