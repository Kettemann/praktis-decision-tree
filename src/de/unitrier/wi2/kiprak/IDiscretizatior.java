package de.unitrier.wi2.kiprak;

import java.util.Collection;

/**
 * Binning discretization for non-numeric values.
 */
public interface IDiscretizatior {

    /**
     * @param numberOfBins defines how many bins should be used for discretization.
     * @param examples     the collection of CSV rows that contains non-numeric values.
     * @param attributeId  the index of the attribute that should be discretized.
     * @param <T>          the generic type parameter of the data.
     * @return the examples list with the specified attribute being discretized.
     */
    public  <T extends Comparable<T>> Collection<T[]> discretize(int numberOfBins, Collection<T[]> examples, int attributeId);
}
