package de.unitrier.wi2.kiprak;

import java.util.Collection;

/**
 * Pruning for decision trees.
 */
public interface IPruner {
    /**
     * Prunes a decision tree.
     * @param examples the collection of CSV rows.
     * @param <T> the generic type parameter of the data.
     * @return the root node of the pruned decision tree.
     */
    public <T extends Comparable<T>> DecisionTreeNode prune(Collection<T[]> examples);
}
