package de.unitrier.wi2.kiprak;

import java.io.IOException;
import java.util.*;

import static de.unitrier.wi2.kiprak.State.*;

/**
 * The Main class reads a CSV file into a list, discretizes numeric values in this list,
 * creates an ID3 decision tree, validates the tree's classification accuracy with 5-fold
 * cross validation, uses post pruning to create an ID3 tree without overfitting.
 * Parameters for many operations are defined in the State class.
 */
public class Main {

    // TODO: Shuffle Methode für die CrossValidation Trainings- und TestDaten
    // TODO: BinningDiscretizator Methodenauslagerung?

    public static void main(String[] args) throws IOException {
        long startTime = System.currentTimeMillis();

        List<String[]> values = CSVReader.readCsvToArray(CSV_PATH, CSV_DELIMITER, true);
        LinkedList<String> headers = CSVReader.readCsvHeadersToArray(CSV_PATH, CSV_DELIMITER);

        BinningDiscretizator binningDiscretizator = new BinningDiscretizator();
        for (int i : DISCRETIZATION_INDICES) {
            values = (List<String[]>) binningDiscretizator.discretize(DISCRETIZATION_NUMBER_OF_BINS, values, i);
        }

        printCrossValidationResults(values, false);

        printTimeElapsed(startTime);

        ReducedErrorPruner pruner = new ReducedErrorPruner();
        DecisionTreeNode id3RootNode = pruner.prune(values);

        printCrossValidationResults(values, true);

        printTimeElapsed(startTime);

        XMLParser.saveToXML(id3RootNode, headers);

        printTimeElapsed(startTime);
    }

    public static void printCrossValidationResults(Collection<String[]> values, boolean usePruning) {
        double classificationAccuracy = CrossValidation.nFoldValidation(CROSS_VALIDATION_K, values, usePruning);
        System.out.println(classificationAccuracy);
    }

    public static void printTimeElapsed(long startTime) {
        String estimatedTime = ((System.currentTimeMillis() - startTime) / 1000) + "," + ((System.currentTimeMillis() - startTime) % 1000);
        System.out.println("seconds elapsed:");
        System.out.println("--- " + estimatedTime + " ---");
    }


}
