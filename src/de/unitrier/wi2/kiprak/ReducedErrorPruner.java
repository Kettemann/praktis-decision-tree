package de.unitrier.wi2.kiprak;

import java.util.*;

import static de.unitrier.wi2.kiprak.CrossValidation.getTestData;
import static de.unitrier.wi2.kiprak.CrossValidation.getTrainingData;
import static de.unitrier.wi2.kiprak.ID3.appendAllID3NodesToThisList;
import static de.unitrier.wi2.kiprak.State.*;

/**
 * Implements post pruning.
 */
public class ReducedErrorPruner implements IPruner {

    /**
     * Creates an ID3 decision tree and applies post pruning.
     *
     * @param examples    the collection of CSV rows.
     * @param <T>         the generic type parameter of the data.
     * @return the pruned ID3 decision tree's root node.
     */
    @Override
    public <T extends Comparable<T>> DecisionTreeNode prune(Collection<T[]> examples) {
        LinkedList<T[]> data = new LinkedList<>(examples);
        if (PRUNING_SHUFFLE_TESTDATA) {
            Collections.shuffle(data);
        }

        // split the data into 5 subsets -> take the first subset containing the first 20% of the data.
        LinkedList<LinkedList<T[]>> testSets = getTestData(5, data);
        LinkedList<T[]> testData = testSets.get(0);
        // take the remaining 4 partitions (last 80%) of examples merged together.
        LinkedList<T[]> trainingData = getTrainingData(5, testSets).get(0);

        return postPruning(trainingData, testData);
    }

    /**
     *
     * @param trainingData the data used to create the ID3 decision tree.
     * @param testData     the data used to validate the ID3 decision tree.
     * @param <T>          the generic type parameter of the data.
     * @return the pruned ID3 decision tree's root node
     */
    public static <T extends Comparable<T>> DecisionTreeNode postPruning(LinkedList<T[]> trainingData, LinkedList<T[]> testData) {
        DecisionTreeNode id3RootNode = ID3.createTree(trainingData, LABEL_INDEX);
        LinkedList<DecisionTreeNode> allNodes = appendAllID3NodesToThisList(id3RootNode, new LinkedList<DecisionTreeNode>());
        LinkedList<String> columnHeaders = CSVReader.readCsvHeadersToArray(CSV_PATH, CSV_DELIMITER);

        double initialAccuracy = -1;
        double currentAccuracy = 0;
        double bestOfLoop = 0;
        DecisionTreeNode bestNodeToRemove = null;

        while (initialAccuracy < bestOfLoop) {
            initialAccuracy = CrossValidation.testResults(testData, id3RootNode);
            for (DecisionTreeNode node : allNodes) {
                if (node.getLeafNodeClass() != null) continue;

                HashMap<String, DecisionTreeNode> backup = node.getLeafs();

                node.setLeafNodeClass(node.getDominantClass());

                currentAccuracy = CrossValidation.testResults(testData, id3RootNode);

                if (currentAccuracy > bestOfLoop) {
                    bestOfLoop = currentAccuracy;
                    bestNodeToRemove = node;
                }

                node.setLeafs(backup);
                node.setLeafNodeClass(null);

            }
            if (bestOfLoop >= initialAccuracy) {
                bestNodeToRemove.setLeafNodeClass(bestNodeToRemove.getDominantClass());
                bestNodeToRemove.setLeafs(null);
                allNodes = appendAllID3NodesToThisList(id3RootNode, new LinkedList<DecisionTreeNode>());
            }
        }
//        System.out.println("post pruning accuracy: " + bestOfLoop);
        return id3RootNode;
    }
}
