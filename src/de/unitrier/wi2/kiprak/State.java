package de.unitrier.wi2.kiprak;

/**
 * Defines all the parameters necessary for the ID3 implementation.
 */
public class State {

    public static final String exampleCSVPath = "data/example.csv";
    public static final String customDataCSVPath = "data/online_shoppers_intention_custom.csv";
    public static final String dataCSVPath = "data/online_shoppers_intention.csv";
    public static final String CSV_PATH = dataCSVPath;
    public static final String CSV_DELIMITER = ",";
    public static final int LABEL_INDEX = 17;

    public static final int CROSS_VALIDATION_K = 5;
    public static final boolean CROSS_VALIDATION_SHUFFLE_TESTDATA = true;

    public static final int DISCRETIZATION_NUMBER_OF_BINS = 10;
    public static final int[] DISCRETIZATION_INDICES = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    public static final boolean PRUNING_SHUFFLE_TESTDATA = true;

    public static final String XML_FILE_NAME = "ID3_decision_tree.xml";
}
