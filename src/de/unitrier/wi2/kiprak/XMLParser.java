package de.unitrier.wi2.kiprak;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import org.w3c.dom.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;

import static de.unitrier.wi2.kiprak.State.XML_FILE_NAME;

/**
 * Create and write XML documents
 */
public class XMLParser {

    /**
     * Saves a decision tree to an XML file.
     *
     * @param node    the root DecisionTreeNode to build the tree.
     * @param headers the CSV column headers.
     */
    public static void saveToXML(DecisionTreeNode node, LinkedList<String> headers) {
        Document dom;
        try {
            // create an XML document and root element
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            dom = documentBuilder.newDocument();
            Element rootEle = dom.createElement("DecisionTree");

            Element treeRoot = createNode(dom, node, rootEle, headers);

            Map<String, DecisionTreeNode> map = node.getLeafs();
            for (Map.Entry<String, DecisionTreeNode> mapEntry :
                    map.entrySet()) {
                appendValue(dom, treeRoot, mapEntry, headers);
            }

            dom.appendChild(rootEle);

            try {
                Transformer tr = TransformerFactory.newInstance().newTransformer();
                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "xml");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                // send the DOM to a file
                tr.transform(new DOMSource(dom),
                        new StreamResult(new FileOutputStream(XML_FILE_NAME)));

            } catch (TransformerException te) {
                System.out.println(te.getMessage());
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            }
        } catch (ParserConfigurationException pce) {
            System.out.println(pce);
        }
    }

    /**
     *
     * @param dom the document for which the node should be created.
     * @param rootNode the root DecisionTreeNode to build the tree.
     * @param mapEntry a mapEntry containing a DecisionTreeNode to include into the XML tree.
     * @param headers the CSV column headers.
     */
    private static void appendValue(Document dom, Element rootNode, Map.Entry<String, DecisionTreeNode> mapEntry, LinkedList<String> headers){

        if (mapEntry.getValue().getLeafNodeClass() != null){

            Element e = dom.createElement("IF");
            e.setAttribute("value", mapEntry.getKey());
            rootNode.appendChild(e);

            Element result = dom.createElement("leafNodeClass");
            result.setAttribute("class", mapEntry.getValue().getLeafNodeClass());
            e.appendChild(result);
            return;
        }
        Element e = dom.createElement("IF");
        e.setAttribute("value", mapEntry.getKey());
        rootNode.appendChild(e);

        Map<String, DecisionTreeNode> map = mapEntry.getValue().getLeafs();
        Element newNode = createNode(dom, mapEntry.getValue(), e, headers);

        for (Map.Entry<String, DecisionTreeNode> child :
                map.entrySet()) {
            appendValue(dom, newNode, child, headers);
        }
    }

    /**
     * Creates an XML decision tree node.
     * @param dom the document for which the node should be created.
     * @param node the DecisionTreeNode which should be represented.
     * @param rootEle the element to append the new XML node.
     * @param headers the CSV column headers.
     * @return the created node.
     */
    private static Element createNode(Document dom, DecisionTreeNode node, Element rootEle, LinkedList<String> headers){
        Element rootNode = dom.createElement("Node");

        int index = node.getAttributeIndex();
        rootNode.setAttribute("attributeId", "" + index);
        rootNode.setAttribute("attribute", headers.get(index));
        rootEle.appendChild(rootNode);
        return rootNode;
    }
}
