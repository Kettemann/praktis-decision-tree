package de.unitrier.wi2.kiprak;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class EntropyTest {

    static ArrayList<String[]> data = new ArrayList<String[]>();

    @BeforeAll
    static  void init(){
        data.add(new String[]{"klein","blond","blau","+"});
        data.add(new String[]{"gross","rot","blau","+"});
        data.add(new String[]{"gross","blond","blau","+"});
        data.add(new String[]{"gross","blond","braun","-"});
        data.add(new String[]{"klein","dunkel","blau","-"});
        data.add(new String[]{"gross","dunkel","blau","-"});
        data.add(new String[]{"gross","dunkel","braun","-"});
        data.add(new String[]{"klein","blond","braun","-"});
    }

    @Test
    void calculateRestEntropyForAttribute() {
        assertEquals(0.9512050593046015, Entropy.calculateRestEntropyForAttribute(0,data,3));
    }

    @Test
    void calculateEntropyForAttributeValue() {
        // einfach eine 1 in die Parameter hinzufügen
        assertEquals(0.9709505944546686, Entropy.calculateEntropyForAttributeValue(0, data, "gross", 3));
        assertEquals(0.9182958340544896, Entropy.calculateEntropyForAttributeValue(0, data, "klein", 3));
        assertEquals(0.9709505944546686, Entropy.calculateEntropyForAttributeValue(0, data, "gross", 3));
        assertEquals(0.9182958340544896, Entropy.calculateEntropyForAttributeValue(0, data, "klein", 3));
    }

    @org.junit.jupiter.api.Test
    void calculateEntropy() {
        assertEquals(0.9544340029249649, Entropy.calculateEntropy(new long[]{3, 5}));
        assertEquals(1 , Entropy.calculateEntropy(new long[]{3,3}));
        assertEquals(0 , Entropy.calculateEntropy(new long[]{3}));
    }

    @Test
    void log2() {
        assertEquals(2,Entropy.log2(4));
        assertEquals(3,Entropy.log2(8));
        assertEquals(0,Entropy.log2(1));
        assertEquals(1.5849625007211563,Entropy.log2(3));
    }
}